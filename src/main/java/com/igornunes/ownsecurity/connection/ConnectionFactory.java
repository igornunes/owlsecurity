package com.igornunes.ownsecurity.connection;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author igor
 */
public class ConnectionFactory {
    
    public void createConnection(){
        Class clazz;
        try {
            clazz = this.getClass().getClassLoader().loadClass("EntityManager");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
