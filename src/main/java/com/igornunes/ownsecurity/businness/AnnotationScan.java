package com.igornunes.ownsecurity.businness;

import com.igornunes.ownsecurity.annotations.OwlPermissionTable;
import com.igornunes.ownsecurity.annotations.OwlRoleTable;
import com.igornunes.ownsecurity.annotations.OwlUserTable;
import java.lang.reflect.Method;

/**
 *
 * @author igor
 */
public class AnnotationScan {

    public static void processAnnotations(Object obj) {
        try {
            Class cl = obj.getClass();
            for (Method m : cl.getDeclaredMethods()) {
                OwlUserTable userTable = m.getAnnotation(OwlUserTable.class);
                OwlRoleTable roleTable = m.getAnnotation(OwlRoleTable.class);
                OwlPermissionTable permissionTable = m.getAnnotation(OwlPermissionTable.class);
                if (userTable != null) {
//                    Field f = cl.getDeclaredField(a.source());
//                    f.setAccessible(true);
//                    addListener(f.get(obj), obj, m);
                } else if (roleTable != null) {
                    
                } else if (permissionTable != null) {
                    
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}