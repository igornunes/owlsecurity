package com.igornunes.ownsecurity.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used to change the default table
 * @author igor
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface OwlUserTable {
    String tablename() default "[user]";
    String name() default "[name]";
    String password() default "[password]";
    String id() default "[id]";
    String joinTableRole() default "[user_role]";
    String foreignKeyRole() default "[id_user]";
}
