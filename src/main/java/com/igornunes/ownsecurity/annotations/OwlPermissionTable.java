package com.igornunes.ownsecurity.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author igor
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface OwlPermissionTable {
    String tablename() default "[permission]";
    String name() default "[name]";
    String id() default "[id]";
    String foreignKeyPermission() default "[id_permission]";
}
