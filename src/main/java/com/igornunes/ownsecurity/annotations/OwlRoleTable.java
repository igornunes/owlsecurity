package com.igornunes.ownsecurity.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author igor
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface OwlRoleTable {
    String tablename() default "[role]";
    String name() default "[name]";
    String id() default "[id]";
    String joinTablePermission() default "[role_permission]";
    String foreignKeyRole() default "[id_role]";
}
